/* eslint react/no-danger: 0 */
import React from 'react';

export default NeueUI => NeueUI.register('views$base:PageStructure', {
	component: ({ children }) => (
		<html lang="en">
			<head>
				<title>Welcome</title>
			</head>
			<body>
				<main dangerouslySetInnerHTML={{ __html: children }} />
				<script src="/app.js" />
			</body>
		</html>
	)
});
