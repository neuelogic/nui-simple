/* global BUILD_ARGS */

import mkdirp from 'mkdirp';
import fs from 'fs';
import path from 'path';
import CompilerFactory from 'nui-build';
import BabelBuilder from 'nui-builder-babel';
import BuildWatchman from 'nui-build-watch';
import Starter from './starter.js';

const startCompiler = (Config) => {
	const BuildConfig = Config.Build;

	const Compiler = CompilerFactory(BuildConfig);

	Compiler.dispatcher.when('NeueClean').then(() => Compiler.copyModules());

	Compiler.dispatcher.when('NeueModulesCopied').then(() =>
		new BabelBuilder(
			BuildConfig.SourcePath,
			BuildConfig.DefaultModulePath,
			Compiler
		)
	).catch(e => Compiler.compileError(e));

	Compiler.dispatcher.when('BabelBuilder').then(() => Compiler.dispatcher.resolve('BuildStart'));

	Compiler.dispatcher.when('PostBuild').then(() => Starter(Config));

	Compiler.init(true, BUILD_ARGS.dump, false);

	return Compiler;
};

const executor = (Config) => {
	const BuildConfig = Config.Build;

	const PageStructLoc = path.join(BuildConfig.SourcePath, 'PageStructure.js');
	const NotFoundLoc = path.join(BuildConfig.SourcePath, 'pages', 'NotFound.js');
	let PageStructReady = false;
	let NotFoundReady = false;

	try {
		fs.accessSync(PageStructLoc);
		PageStructReady = true;
	} catch (e) {
		mkdirp.sync(path.resolve(BuildConfig.DefaultModulePath));
		fs.createReadStream(path.join(__dirname, './default/PageStructure.js')).pipe(
			fs.createWriteStream(path.join(BuildConfig.DefaultModulePath, 'PageStructure.js')).on('finish', () => {
				PageStructReady = true;
			})
		);
	}

	try {
		fs.accessSync(NotFoundLoc);
		NotFoundReady = true;
	} catch (e) {
		mkdirp.sync(
			path.join(
				path.resolve(BuildConfig.DefaultModulePath), 'pages'
			)
		);
		fs.createReadStream(path.join(__dirname, './default/NotFound.js')).pipe(
			fs.createWriteStream(path.join(BuildConfig.DefaultModulePath, 'pages', 'NotFound.js')).on('finish', () => {
				NotFoundReady = true;
			})
		);
	}

	const WaitInt = setInterval(() => {
		if (NotFoundReady !== true && PageStructReady !== true) return;

		clearInterval(WaitInt);
		try {
			startCompiler(Config);
		} catch (e) {
			console.log(e);
		}
	}, 100);
};

export default (Config) => {
	const BuildConfig = Config.Build;

	if (BUILD_ARGS.w) {
		return new BuildWatchman(
			BuildConfig.BuildContext,
			BuildConfig.SourcePath,
			() => executor(Config)
		);
	}
	return executor(Config);
};
