#!/usr/bin/env node

/* global BUILD_ARGS */
import argv from 'yargs';
import mkdirp from 'mkdirp';
import generateConfig from './config.js';
import Builder from './builder.js';
import Starter from './starter.js';

Object.defineProperty(global, 'BUILD_ARGS', {
	value: argv
		.alias('b', 'build')
		.boolean('b')

		.alias('c', 'clean')
		.boolean('c')

		.alias('w', 'watch')
		.boolean('c')

		.alias('t', 'target')
		.default('t', 'all')

		.alias('l', 'loglevel')
		.choices('l', ['quiet', 'loud', 'debug'])
		.default('l', 'quiet')

		.alias('C', 'config')

		.alias('D', 'dump')
		.boolean('D')
		.default('D', false)
	.argv
});

const Config = generateConfig(BUILD_ARGS);

if (BUILD_ARGS.b) {
	mkdirp(Config.Build.NuiOutputFolder, (err) => {
		if (err) throw Error(err);
		Builder(Config);
	});
} else Starter(Config);
